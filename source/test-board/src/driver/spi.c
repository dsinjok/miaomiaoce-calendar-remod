#include "spi.h"
#include "platform_systick.h"

void spi_send_byte(uint8_t byte)
{
    /* loop while data register in not emplty */
    while (RESET == spi_i2s_flag_get(SPI2,SPI_FLAG_TBE));

    /* send byte through the SPI0 peripheral */
    spi_i2s_data_transmit(SPI2, byte);
    delay_us(1);
}

void spi_config() {
    rcu_periph_clock_enable(RCU_AF);
    // RES(PD0),  D/C(PD1): data-1 command-0  
    gpio_pin_remap_config(GPIO_PD01_REMAP, ENABLE);
    rcu_periph_clock_enable(RCU_GPIOD);
    gpio_init(GPIOD, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_0);
    gpio_init(GPIOD, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_1);
    gpio_bit_reset(GPIOD, GPIO_PIN_0);
    gpio_bit_reset(GPIOD, GPIO_PIN_1);

    // Disable JTAG, remap PB3,PB4,PA15
    gpio_pin_remap_config(GPIO_SWJ_SWDPENABLE_REMAP, ENABLE);
    
    rcu_periph_clock_enable(RCU_GPIOA);
    rcu_periph_clock_enable(RCU_GPIOB);
    rcu_periph_clock_enable(RCU_SPI2);

    /* SPI2_SCK(PB3) and SPI2_MOSI(PB5) GPIO pin configuration */
    gpio_init(GPIOB, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_3 | GPIO_PIN_5);
    /* BUSY(PB4) input pin */
    gpio_init(GPIOB, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ, GPIO_PIN_4);
    /* SPI2_CS(PA15) GPIO pin configuration */
    gpio_init(GPIOA, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_15);
    
    /* chip select invalid*/
    gpio_bit_set(GPIOA, GPIO_PIN_15);
    
    /* BUS (PA12) 4-line SPI-0, 3-lines SPI-1  */
    gpio_init(GPIOA, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_12);
    gpio_bit_reset(GPIOA, GPIO_PIN_12);

    spi_parameter_struct spi_init_struct;
    /* SPI0 parameter config */
    spi_init_struct.trans_mode           = SPI_TRANSMODE_FULLDUPLEX;
    spi_init_struct.device_mode          = SPI_MASTER;
    spi_init_struct.frame_size           = SPI_FRAMESIZE_8BIT;;
    spi_init_struct.clock_polarity_phase = SPI_CK_PL_LOW_PH_1EDGE;
    spi_init_struct.nss                  = SPI_NSS_SOFT;
    spi_init_struct.prescale             = SPI_PSC_2;
    spi_init_struct.endian               = SPI_ENDIAN_MSB;;
    spi_init(SPI2, &spi_init_struct);

    /* enable SPI0 */
    spi_enable(SPI2);
}