Import("env")

platform = env.PioPlatform()

env.Prepend(
    UPLOADERFLAGS=["-s", platform.get_package_dir("tool-openocd-gd32") or ""]
)
env.Append(
    UPLOADERFLAGS=["-c", "program {$SOURCE} 0x08000000 verify reset exit"]
)
env.Replace(
    UPLOADER="openocd",
    UPLOADCMD="$UPLOADER $UPLOADERFLAGS"
)