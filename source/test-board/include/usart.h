#ifndef USART_H
#define USART_H

#include <stdio.h>

void usart_init();

int fgetc(FILE*f);

int fputc(int ch, FILE *f);

#endif