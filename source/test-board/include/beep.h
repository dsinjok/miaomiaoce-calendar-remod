#ifndef BEEP_H
#define BEEP_H

void beep_init();
void beep_setup(int enable);
void beep_on();
void beep_off();
void beep();

#endif