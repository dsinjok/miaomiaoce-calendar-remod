#ifndef SPI_H
#define SPI_H

#include "gd32f30x.h"

void spi_config();

void spi_send_byte(uint8_t byte);

#endif