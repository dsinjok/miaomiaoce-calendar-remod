#ifndef RX8130CE_H
#define RX8130CE_H

#include "gd32f30x.h"

#define RX8130_SLAVE_ADDR 0x64

/* RX8130CE Structure for date/time */
typedef struct {
  uint8_t seconds; /*!< Seconds parameter, from 00 to 59 */
  uint8_t minutes; /*!< Minutes parameter, from 00 to 59 */
  uint8_t hours;   /*!< Hours parameter, 24Hour mode, 00 to 23 */
  uint8_t week;    /*!< Day in a week, from 0 to 6 (Sunday to Saturday) */
  uint8_t day;     /*!< Day in a month, 1 to 31 */
  uint8_t month;   /*!< Month in a year, 1 to 12 */
  uint8_t year;    /*!< Year parameter, 00 to 99, 00 is 2000 and 99 is 2099 */
} rx8130ce_time_t;

void rx8130ce_init(void);

int rx8130ce_set_time(const rx8130ce_time_t *buf);

int rx8130ce_get_time(rx8130ce_time_t *buf);

#endif